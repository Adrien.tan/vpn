# VPN



## Pour qui est fait le projet ?
Une entreprise composé de 10 machines avec 3 services

> - Service Comptabilité
> - Service Technique
> - Service Communication

## Ce que souhaite les utilisateurs de votre projet ? 
Pouvoir accéder au réseau de l'entreprise, où chaque service accède à ses données

## Ce qu’est votre projet
Mise en place d'un VPN pour entreprise basé sur OpenVPN

## Les fonctionnalités essentielles 
Permet d'avoir accès au réseau de l'entreprise peut importe où l'on se trouve grâce à une connexion internet et un tunnel VPN sécurisé qui permet chiffrer les envoies de données entre l'utilisateur et l'entreprise

## Autres solutions concurrentes 
>
> - Cisco VPN : <https://www.cisco.com/c/fr_fr/products/security/vpn-endpoint-security-clients/index.html>
> - VPN Celest : <https://www.celeste.fr/connexions-et-reseaux/reseaux-vpn/>
>

## Comment vous vous différenciez par rapport aux autres solutions 
Par le coût, openVPN est un un VPN open source. Nous facturons uniquement la main d'oeuvre et notre service support est joignable du lundi au vendredi de 8h à 20h et le samedi de 9h à 14h.

## Durée de la réalisation du projet
```
Calcul de la durée du projet : somme des points / capacité initiale = nombres de sprints
75/50 = 1,5
Notre projet sera donc réalisé en 2 jours
```
